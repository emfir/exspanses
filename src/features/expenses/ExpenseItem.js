import {Button, Form} from "react-bootstrap";
import {useDispatch} from "react-redux";
import {removeExpense, updateExpense} from "./expensesSlice";
import {useState} from "react";

const ExpenseItem = ({number, entity, onChange}) => {
    const {id, title, price} = entity;

    const [tempP, setTempP] = useState(price);
    const dispatch = useDispatch();

    const onRemoveClick = () => {
        dispatch(removeExpense(id))

    }
    const onInput = ({target}) => {
        setTempP(target.value)
    }
    const onUpdate = () =>
        dispatch(updateExpense({id: id, changes: {price: tempP}}));

    return <tr>
        <td>{number}</td>
        <td>{title}</td>
        <td><Form.Control placeholder="Title of the expense" value={tempP} onInput={onInput}/>
        </td>
        <td style={{"display": "flex", "alignItems": "center", "justifyContent": "space-around"}}>
            <div>
                <Button variant="success" onClick={onUpdate}>Update</Button>

                <Button variant="danger" onClick={onRemoveClick}>
                    remove
                </Button>
            </div>
            <Form.Check onChange={() => onChange(id)} type="checkbox"/>

        </td>
    </tr>

}


export default ExpenseItem;