import {Button, Col, Container, Form, Row, Table} from "react-bootstrap";
import {useState} from "react";
import {addExpense, getAllExpenses, removeExpenses} from "./expensesSlice";
import {useDispatch, useSelector} from "react-redux";
import ExpenseItem from "./ExpenseItem";

const ExpensesMainView = () => {
    const [expenseTitle, setExpenseTitle] = useState('');
    const [expensePrice, setExpensePrice] = useState('')
    const dispatch = useDispatch();
    const expenses = useSelector(getAllExpenses);
    console.log(expenses)
    const [selectedIds, setSelectedIds] = useState([])

    const onChange = id => {
        if (selectedIds.includes(id)) {
            setSelectedIds(selectedIds.filter(id_f => id_f !== id));
        } else {
           setSelectedIds([...selectedIds, id]);
        }
        console.log(selectedIds);
    }
    const removeSelected = () => {
        dispatch(removeExpenses(selectedIds));
        setSelectedIds([]);
    }

    const onChangeTitle = ({target}) => {
        setExpenseTitle(target.value)
    }

    const onChangePrice = ({target}) => {
        setExpensePrice(target.value)
    }

    const onClick = () => {

        dispatch(addExpense({title: expenseTitle, price: expensePrice}));
    }

    return <Container>
        <Form style={{"padding": "20px"}}>
            <Row style={{"margin": "20px"}}>
                <Col xs={7}>
                    <Form.Control placeholder="Title of the expense" onInput={onChangeTitle}/>
                </Col>
                <Col>
                    <Form.Control placeholder="Price" onInput={onChangePrice}/>
                </Col>
                <Col>
                    <Button variant="primary" onClick={onClick}>Add</Button>
                </Col>
            </Row>
            <Row>
                <Button variant="danger" onClick={removeSelected}>Remove selected</Button>
            </Row>

        </Form>

        <Table striped bordered hover>
            <thead>
            <tr>
                <th>#</th>
                <th>title</th>
                <th>price</th>
                <th>options</th>
            </tr>
            </thead>
            <tbody>
            {expenses.map((expense, id) => <ExpenseItem key={expense.id} number={id} entity={expense} onChange={onChange}/>)}
            </tbody>

        </Table>
    </Container>;
}

export default ExpensesMainView;