import React from 'react';
import './App.css';
import ExpensesMainView from "./features/expenses/ExpensesMainView";

function App() {
    return (
        <ExpensesMainView/>
    );
}

export default App;
